package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chat")
public class ChatController {
    private final ChatService chatService;
    private final AuthService authService;

    @GetMapping({""})
    public ResponseEntity<?> getAll(@RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(chatService.getAll());
        }
        return null;
    }

    @PostMapping({""})
    public ResponseEntity<?> addChat(@RequestBody Chat chat, @RequestHeader("auth") String token){
            if (authService.isExistByToken(token)) {
                return ResponseEntity.ok(chatService.addChat(chat));
            }
        return null;
    }

    @DeleteMapping({"/deleteChat"})
    public void delete(@RequestBody Chat chat, @RequestHeader("auth") String token) {
                if (authService.isExistByToken(token)) {
                    chatService.delete(chat);
                }
    }

}
