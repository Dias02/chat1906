package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
@Component
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final ParticipantRepository participantRepository;
    private final AuthService authService;


    @GetMapping("/getAll")
    public ResponseEntity<?>getAll(){
        return ResponseEntity.ok(messageService.getAll());
    }


    @PostMapping({"/addNewMessage"})
    public ResponseEntity<?> addMessage(@RequestBody Message message,
                                        @RequestHeader("auth") String token) throws Exception {
            if (authService.isExistByToken(token))
        {
            Date date = new Date();
            message.setCreatedTimestamp(date.getTime());
            message.setUpdatedTimestamp(date.getTime());
            message.setDelivered(false);
            message.setRead(false);
            message.setMessageType("basic");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(messageService.addMessage(message));
    }

    @PutMapping({"/update"})
    public ResponseEntity<?> updateMessage(@RequestBody Message message)
            throws Exception
    {
        return ResponseEntity.ok(messageService.updateMessage(message));
    }


    @DeleteMapping({"/deleteMessage"})
    public void delete(@RequestBody Message message) {
        messageService.delete(message);
    }


    @GetMapping("/getMessageByChatId/{id}")
    public ResponseEntity<?> getLastMessageByChatId(@PathVariable Long id, @RequestHeader  Long userId, @RequestHeader("auth") String token)  {
        if (authService.isExistByToken(token)) {
            User user = authService.getUserByToken(token);
            if (!participantRepository.existsByChatIdAndUserId(id, user.getId())) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            List<Message> messageList = messageService.findByChatIdAndUserIdNot(id, userId);
            Date date = new Date();

            for (Message message : messageList) {

                if (!message.isDelivered()) {
                    message.setDelivered(true);
                    message.setDeliveredTimestamp(date.getTime());
                    messageService.update(message);
                }
            }
            return ResponseEntity.ok(messageService.getLastMessageByChatId(id));
        }
        return ResponseEntity.ok("login!");
    }

    @GetMapping("/read/{id}")
    public ResponseEntity<?> readMessage(@PathVariable Long id, @RequestHeader Long userId, @RequestHeader("auth") String token)  {
        if (authService.isExistByToken(token)) {
            User user = authService.getUserByToken(token);
            if (!participantRepository.existsByChatIdAndUserId(id, user.getId())) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            List<Message> messageList = messageService.findByChatIdAndUserIdNot(id, userId);
            Date date = new Date();

            for (Message message : messageList) {
                if (!message.isRead()) {
                    message.setRead(true);
                    message.setReadTimestamp(date.getTime());
                    messageService.update(message);
                }
            }
            return ResponseEntity.ok(messageService.getLastMessageByChatId(id));
        }
        return ResponseEntity.ok("login!");
    }

    @GetMapping("getMessageByUserId/{id}")
    public ResponseEntity<?> getLastMessageByUserId(@PathVariable Long id,@RequestHeader("auth") String token){
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(messageService.getLastMessageByUserId(id));
        }
        return null;
    }


    @GetMapping({"/chat/{id}"})
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long id,@RequestParam(value = "size",required = false) Integer size,@RequestHeader("auth") String token){
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(messageService.getMessagesByChatId(id,size));
        }
        return null;
    }

    @GetMapping("/getAllNotDeliveredByChat/{id}")
    public ResponseEntity<?> getNotDelivered(@PathVariable Long id, @RequestHeader("auth") String token){
        if (authService.isExistByToken(token)) {
            return  ResponseEntity.ok(messageService.getAllNotDeliveredByChat(id));
        }
        return null;
    }

    @PostMapping("/status/{chatId}")
    public ResponseEntity<?> status(@RequestHeader("userId") Long userId, @RequestHeader("status") String status, @PathVariable Long chatId, @RequestHeader("auth") String token) throws Exception {
        if (authService.isExistByToken(token)) {
            Message message = new Message();
            message.setUserId(userId);
            message.setChatId(chatId);
            if (status.equals("writing")) {
                message.setMessageType("writing");
                message.setText("user writing message");
                messageService.addMessage(message);
                return ResponseEntity.ok(message);
            } else if (status.equals("online")) {
                message.setMessageType("online");
                message.setText("user online");
                messageService.addMessage(message);
                return ResponseEntity.ok(message);
            }
        }
        return ResponseEntity.ok("Error");
    }

}
