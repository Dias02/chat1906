package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping("/api/v1/participant")
public class ParticipantController {

    private final ParticipantService participantService;
    private final AuthService authService;

    @GetMapping("/getAll")
    public ResponseEntity<?> getAll(@RequestHeader("auth") String token){
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(participantService.getAll());
        }
        return null;
    }

    @DeleteMapping({"/deleteParticipant"})
    public void delete(@RequestBody Participant participant,@RequestHeader("auth") String token) {
            if (authService.isExistByToken(token)) {
                participantService.delete(participant);
            }
    }

    @GetMapping({"/chat/{id}"})
    public ResponseEntity<?> getParticipantByChatId(@PathVariable(name = "id") Long chatId, @RequestHeader("auth") String token){
                if (authService.isExistByToken(token)) {
                    return ResponseEntity.ok(participantService.getUsersByChat(chatId));
                }
        return null;
    }

    @GetMapping({"/user/{id}"})
    public ResponseEntity<?> getParticipantByUserId(@PathVariable(name = "id") Long userId, @RequestHeader("auth") String token){
                    if (authService.isExistByToken(token)) {
                        return ResponseEntity.ok(participantService.getParticipantByUser(userId));
                    }
        return null;
    }

}
