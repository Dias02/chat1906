package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatRepository extends JpaRepository <Chat, Long> {

}
