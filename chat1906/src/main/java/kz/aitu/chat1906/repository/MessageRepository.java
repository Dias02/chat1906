package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository <Message, Long> {
//    List<Message> findAllMessagesByChatId(Long chatId);

    List<Message> findByChatId(Long chatId, Pageable pageable);
    List<Message> findByChatIdAndUserIdNot(Long chatId,Long userId);
//    List<Message> getByDeliveredIsFalse(Long chatId);

    @Query(value = "SELECT * FROM message m WHERE m.chat_id = ?1 ORDER BY m.id DESC LIMIT 10", nativeQuery=true)
    List<Message> getLastMessagesByChatId(Long chatId);
    @Query(value = "SELECT * FROM message m WHERE m.user_id = ?1 ORDER BY m.id DESC LIMIT 10", nativeQuery=true)
    List<Message> getLastMessageByUserId(Long userId);
    @Query(value = "SELECT * from message m where m.chat_id = ?1 and is_delivered = false order by m.id desc ", nativeQuery=true)
    List<Message> findByChatIdAndDeliveredFalse(Long chatId);

}
