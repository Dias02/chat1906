package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    List<Participant> findAllByChatId(Long chatId);
    List<Participant> findAllByUserId(Long userId);
    boolean existsByChatIdAndUserId(Long chatId,Long userId);

    Boolean existsByUserIdAndChatId(Long userId,Long chatId);

}
