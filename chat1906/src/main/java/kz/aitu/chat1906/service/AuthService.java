package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.AuthRepository;
import kz.aitu.chat1906.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {

    private final AuthRepository authRepository;
    private final UserRepository userRepository;

    public void login(Auth auth) {
        UUID uuid = UUID.randomUUID();
        String uuidAsString = uuid.toString();
        Date date = new Date();
        auth.setToken(uuidAsString);
        auth.setLastLoginTimestamp(date.getTime());
        authRepository.save(auth);
    }

    public Boolean isExistByToken(String token){
        return authRepository.existsByToken(token);
    }

    public User getUserByToken(String token){
        Auth auth = authRepository.getByToken(token);
        return userRepository.getUserById(auth.getUserId());
    }
}
