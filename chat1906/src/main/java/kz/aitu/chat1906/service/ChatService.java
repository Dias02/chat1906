package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.ChatRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatService {
    private ChatRepository chatRepository;

    public List<Chat> getAll(){
        return chatRepository.findAll();
    }

    public Chat addChat(Chat chat){
        return chatRepository.save(chat);
    }

    public void delete(Chat chat) {
        this.chatRepository.delete(chat);
    }


}
