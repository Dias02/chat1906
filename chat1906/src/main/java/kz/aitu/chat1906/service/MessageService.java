package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.MessageRepository;
import kz.aitu.chat1906.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;
    private final ParticipantRepository participantRepository;

    public List<Message> getAll() {
        return messageRepository.findAll();
    }

    public void delete(Message message) {
        this.messageRepository.delete(message);
    }

    public Message addMessage( Message message) throws Exception {

        if (message.getText().isEmpty() && message.getText().isBlank())
            throw  new Exception ("Message is not exist");

        if (!participantRepository.existsByUserIdAndChatId(message.getUserId(),message.getChatId()))
            throw  new Exception("User not exist in chat");

        if (message.getUserId() == null && message.getChatId() == null)
            throw new Exception("userId && chatId is not exist");


        Date date = new Date();
        message.setCreatedTimestamp(date.getTime());
        messageRepository.save(message);
        return messageRepository.save(message);
    }

    public Message updateMessage(Message message) throws Exception {

        if(message.getId() == null)
            throw  new Exception("Message id is not exist");

        if(message.getText() == null || message.getText().isEmpty())
            throw new Exception("text is not exist");

        Optional<Message> checkMessage = messageRepository.findById(message.getId());
        if (checkMessage.isEmpty())
            throw new Exception("message is not exist");

        message.setCreatedTimestamp(checkMessage.get().getCreatedTimestamp());


        Date date = new Date();
        message.setUpdatedTimestamp(date.getTime());
        messageRepository.save(message);
        return messageRepository.save(message);
    }



    public List<Message> getLastMessageByChatId(Long id){
        return messageRepository.getLastMessagesByChatId(id);
    }

    public List<Message> getLastMessageByUserId(Long id){
        return messageRepository.getLastMessageByUserId(id);
    }

    public List<Message> getMessagesByChatId(Long id, Integer size) {
        List<Message> messageList = this.messageRepository.findByChatId( id, PageRequest.of(1,size, Sort.by(Sort.Direction.DESC , "CreatedTimestamp")).first());
        return messageList;

    }

    public List<Message> findByChatIdAndUserIdNot(Long chatId,Long userId){
        return messageRepository.findByChatIdAndUserIdNot(chatId,userId);
    }

    public void update(Message message){
        messageRepository.save(message);
    }

    public List<Message> getAllNotDeliveredByChat(Long id){
        return messageRepository.findByChatIdAndDeliveredFalse(id);
    }
}
